---
title: "جلسه ۱۴۹"
description: "میزگرد با موضوع نرم‌افزار آزاد و بازار کار آن در ایران"
date: "1397-05-08"
author: "فرزانه نجفی"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster149.jpg"
readmore: false
---
[![poster149](../../img/posters/poster149.jpg)](../../img/poster149.jpg)

