---
title: "جلسه ۱۷۶"
description: "مستقل سازی اپلیکیشن از زیرساخت با استفاده از داکر"
date: "1398-07-03"
author: "مریم بهزادی"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster176.jpg"
readmore: false
---
[![poster176](../../img/posters/poster176.jpg)](../../img/poster176.jpg)