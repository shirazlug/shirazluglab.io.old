---
title: "بابک رزمجو"
summaryImage: "/img/team/razmjoo.svg"
weight: 1
---
![بابک رزمجو](/img/team/razmjoo.svg)

## تحصیلات
* کارشناسی ریاضی محض

## تاریخچهٔ آشنایی با گنو/لینوکس و شیراز لاگ
* اولین نصب Redhat linux در کنار ویندوز ۱۳۸۴
* نصب توزیعهای shabdix, ubuntu, parsix, fedora در فاصلهٔ ۱۳۸۶ تا ۱۳۹۷
* فرمت کردن :C و نصب فدورا به طور کامل ۱۳۹۶
* شرکت در جلسات شیراز لاگ از زمستان ۱۳۹۶

## تجارب
* Pascal, C, C++, python, java, javascript
* developing for android
* developing static websites

## فعالیتهای حال حاضر
* برنامه نویس اندروید در شرکت اورنگ پژوهان پارسه
* توسعه و نوشتن وبلاگ با استفاده از hugo

